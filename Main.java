﻿package demojavafx;

import javafx.application.Application;
import javafx.scene.Scene;
import javafx.stage.Stage;

public class Main extends Application {
	
	// プログラム実行時に初めにここが呼び出される
	@Override
	public void start(Stage stage) throws Exception {
		// App0クラスをインスタンス化して画面に表示する
		App0 app0 = new App0();
		Scene scene = new Scene(app0);
		stage.setScene(scene);
		stage.show();
	}

}
