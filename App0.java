﻿package demojavafx;

import java.io.IOException;
import javafx.beans.binding.Bindings;
import javafx.beans.binding.BooleanBinding;
import javafx.beans.binding.StringBinding;
import javafx.beans.property.BooleanProperty;
import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.control.RadioButton;
import javafx.scene.control.Slider;
import javafx.scene.control.TextField;
import javafx.scene.layout.BorderPane;

public class App0 extends BorderPane {

	@FXML
	private Slider sliderA;

	@FXML
	private Slider sliderB;

	@FXML
	private TextField valueATextField;

	@FXML
	private TextField valueBTextField;

	@FXML
	private TextField conditionTextField;

	@FXML
	private TextField countTextField;

	@FXML
	private RadioButton countingRadioButton;
	
	// sliderAの値 < sliderBの値 かどうか
	private final BooleanBinding isALessThanB;

	// sliderAの値 < sliderBの値 になった回数
	private final IntegerProperty count = new SimpleIntegerProperty();
	
	// sliderAの値 < sliderBの値 になった回数 をカウントするかどうか
	private final BooleanProperty counting = new SimpleBooleanProperty(true);
	
	public App0() throws IOException {

		// GUIパーツの初期化
		FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("App0.fxml"));
		fxmlLoader.setRoot(this);
		fxmlLoader.setController(this);
		fxmlLoader.load();

		// valueATextFieldのテキスト に sliderAの値をバインドする
		// （sliderAの値が変わると自動的にvalueATextFieldのテキストも変わる）
		this.valueATextField.textProperty().bind(this.sliderA.valueProperty().asString());

		// valueBTextFieldのテキスト に sliderBの値をバインドする
		// （sliderBの値が変わると自動的にvalueBTextFieldのテキストも変わる）
		this.valueBTextField.textProperty().bind(this.sliderB.valueProperty().asString());

		// sliderAの値 < sliderBの値 かどうか
		// （sliderAやsliderBの値の変化に応じて真理値が自動的に変わる）
		this.isALessThanB = this.sliderA.valueProperty().lessThan(this.sliderB.valueProperty());

		// sliderAの値 < sliderBの値　ならば "A is less than B" そうでなければ "A is not less than B" と表示
		// （真理値の変化に応じて自動的にテキストが変わる）
		StringBinding conditionText = Bindings.when(this.isALessThanB).then("A is less than B").otherwise("A is not less than B");
		this.conditionTextField.textProperty().bind(conditionText);

		// countTextFieldのテキストにcountの値をバインドする
		// （countの値が変わると自動的にcountTextFieldのテキストも変わる）
		this.countTextField.textProperty().bind(this.count.asString());
		
		// countingRadioButton の値 と counting の値を「双方向バインデンド」する
		// （countingRadioButton の値が変わると自動的に counting の値も変わり，また，counting の値が変わるとcountingRadioButton の値も自動的に変わる）
		this.countingRadioButton.selectedProperty().bindBidirectional(this.counting);
		
		// 論理式(sliderAの値 < sliderBの値)の真理値が変わる度に呼び出される処理
		this.isALessThanB.addListener((unuse, oldValue, newValue) -> {
			// oldValue : 論理式(sliderAの値 < sliderBの値)の直前の真理値
			// newValue : 論理式(sliderAの値 < sliderBの値)の現在の真理値
			if( newValue && this.counting.get()){
				this.count.set(this.count.get() + 1);
			}
		});
	}

	// onボタンが押された時に呼ばれる
	@FXML
	public void on() {
		counting.setValue(true);
	}

	// offボタンが押された時に呼ばれる
	@FXML
	public void off() {
		counting.setValue(false);
	}
}
